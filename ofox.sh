#!/bin/bash
TARGET_DEVICE=RMX2001
# configure some default settings for the build
Default_Settings() {
export ALLOW_MISSING_DEPENDENCIES=true
export OF_TWRP_COMPATIBILITY_MODE="1"
export OF_DISABLE_MIUI_SPECIFIC_FEATURES="1"
export OF_VANILLA_BUILD="1"
export OF_SCREEN_H="2400"
export OF_STATUS_H="102"
export OF_STATUS_INDENT_LEFT="48"
export OF_STATUS_INDENT_RIGHT="48"
export OF_SKIP_ORANGEFOX_PROCESS="1"
export OF_SUPPORT_OZIP_DECRYPTION="1"
export FOX_USE_BASH_SHELL="1"
export FOX_USE_NANO_EDITOR="1"
export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER="1"
export LC_ALL="C"
export OF_DISABLE_MIUI_SPECIFIC_FEATURES="1"
export OF_FLASHLIGHT_ENABLE="1"
export OF_MAINTAINER="ManshuTyagi"
export OF_NO_TREBLE_COMPATIBILITY_CHECK="1"
export OF_PATCH_AVB20="1"
export OF_TARGET_DEVICES="RMX2001""RMX2002""wasabi"
export TARGET_ARCH="arm64"
export TW_DEVICE_VERSION="Q10.0"
export USE_CCACHE="1"


}

# build the project
do_build() {
  Default_Settings

  # use ccache ??
  [ "$USE_CCACHE" = "1" ] && ccache -M 20G

  # compile it
  . build/envsetup.sh

  lunch omni_"$TARGET_DEVICE"-eng

  mka recoveryimage -j$(nproc --all) && cd device/realme/RMX2001
}

# --- main --- #
cd .. && cd .. && cd .. && do_build
#
